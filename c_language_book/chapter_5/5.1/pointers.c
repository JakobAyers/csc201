#include <stdio.h>

main()
{
int x = 1, y = 2, z[10];
int *ip;
ip = &x;

printf("y is %d\nx is %d\nz is %d\n", x, y, z);

y = *ip;
*ip = 0;
ip = &z[0];

printf("\ny is now %d\nx is now %d\nz is now %d\n", x, y, z);

}
